#!/bin/bash
echo '___________________________________________'
echo 'Welcome to the Fail2Ban deployment service'
echo 'https://gitlab.com/m0ze1r/fail2ban.git'
echo 'debian needs this to be ran as root because'
echo 'of permission issues, please ensure you are running as root'
echo '___________________________________________'
sleep 5
##############
sleep 1
##############
echo '___________________'
echo 'installing fail2ban'
echo '___________________'
sudo apt install fail2ban -y
##############
sleep 2
##############
echo '_______________________'
echo 'adding fail2ban for ssh'
echo '_______________________'
##############
sleep 1
##############
cat <<EOF > /etc/fail2ban/jail.d/sshd.conf
[sshd]
enabled = true
port = ssh
port = 22853
filter = sshd
logpath = /var/log/auth.log
maxretry = 3
bantime = 1800 # 30 minutes
ignoreip = whitelist-IP
EOF
##############
sleep 1
##############
echo '_________________________'
echo 'adding fail2ban for nginx'
echo '_________________________'
##############
cat <<EOF > /etc/fail2ban/jail.d/nginx.conf
[nginx-auth]
enabled = true
filter = nginx-auth
action = iptables-multiport[name=NoAuthFailures, port="http,https"]
logpath = /var/log/nginx*/*error*.log
bantime = 1800 # 30 minutes
maxretry = 6

[nginx-login]
enabled = true
filter = nginx-login
action = iptables-multiport[name=NoLoginFailures, port="http,https"]
logpath = /var/log/nginx*/*access*.log
bantime = 1800 # 30 minutes
maxretry = 6
 
[nginx-badbots]
enabled  = true
filter = apache-badbots
action = iptables-multiport[name=BadBots, port="http,https"]
logpath = /var/log/nginx*/*access*.log
bantime = 604800 # 1 week
maxretry = 1
 
[nginx-noscript]
enabled = true
action = iptables-multiport[name=NoScript, port="http,https"]
filter = nginx-noscript
logpath = /var/log/nginx*/*access*.log
maxretry = 6
bantime  = 86400 # 1 day
 
[nginx-proxy]
enabled = true
action = iptables-multiport[name=NoProxy, port="http,https"]
filter = nginx-proxy
logpath = /var/log/nginx*/*access*.log
maxretry = 7
bantime  = 86400 # 1 day
EOF
##############
sleep 1
##############
echo 'exiting root environment'
exit
sleep 1
##
echo '______________________________'
echo 'enabling fail2ban as a service'
echo '______________________________'
sudo systemctl enable --now fail2ban
##############
sleep 1
##############
echo '_________________________________________'
echo 'Fail2Ban is now deployed, Have a nice day'
echo '_________________________________________'
